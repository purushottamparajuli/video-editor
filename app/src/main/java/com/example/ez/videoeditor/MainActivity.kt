package com.example.ez.videoeditor

import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.ez.videoeditor.BitmapToVideoEncoder.IBitmapToVideoEncoderCallback
import com.googlecode.mp4parser.BasicContainer
import com.googlecode.mp4parser.authoring.Movie
import com.googlecode.mp4parser.authoring.Track
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator
import com.googlecode.mp4parser.authoring.tracks.AppendTrack
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File
import java.io.FileOutputStream
import java.io.RandomAccessFile
import java.util.*


class MainActivity : AppCompatActivity() {

    private val REQUEST_TAKE_GALLERY_VIDEO_1 = 1
    private val REQUEST_TAKE_GALLERY_VIDEO_2 = 2
    private val REQUEST_TAKE_GALLERY_IMAGE = 3
    private var selectedVideoPath1 = ""
    private var selectedVideoPath2 = ""
    private var selectedImagePath = ""
    private var videoUri1: Uri? = null
    private var videoUri2: Uri? = null
    private var imageUri: Uri? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        first.setOnClickListener {
            val intent = Intent()
            intent.type = "video/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_TAKE_GALLERY_VIDEO_1)
        }
        second.setOnClickListener {
            val intent = Intent()
            intent.type = "video/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Select Video"), REQUEST_TAKE_GALLERY_VIDEO_2)
        }

        image.setOnClickListener {
            val intent = Intent()
            intent.type = "image/*"
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Select Image"), REQUEST_TAKE_GALLERY_IMAGE)
        }

        btn_video_image.setOnClickListener {
            val path = getRealPathFromURI(imageUri!!, "imageToVideo")
            val picBitmap = BitmapFactory.decodeFile(path)
            val bitmapToVideoEncoder = BitmapToVideoEncoder(object : IBitmapToVideoEncoderCallback {
                override fun onEncodingComplete(outputFile: File?) {
                    runOnUiThread {
                        Toast.makeText(this@MainActivity, "Encoding complete!", Toast.LENGTH_LONG).show()
                    }
                }
            })
            (1..20).forEach {
                bitmapToVideoEncoder.queueFrame(picBitmap)
            }
            bitmapToVideoEncoder.startEncoding(picBitmap.width, picBitmap.height, File(Environment.getExternalStorageDirectory().toString(), "videoFromImage.mp4"))
//            bitmapToVideoEncoder.stopEncoding()
        }

        btn_merge2.setOnClickListener {
            // video from image
            val path = getRealPathFromURI(imageUri!!, "imageToVideo")
            val picBitmap = BitmapFactory.decodeFile(path)
            val file = File(Environment.getExternalStorageDirectory().toString(), "videoFromImage.mp4")
            val bitmapToVideoEncoder = BitmapToVideoEncoder(object : IBitmapToVideoEncoderCallback {
                override fun onEncodingComplete(outputFile: File?) {
                    picBitmap.recycle()
                    runOnUiThread {
                        /// video merge
                        val inMovies = arrayOfNulls<Movie>(2)
                        inMovies[0] = MovieCreator.build(getRealPathFromURI(videoUri1!!, "firstFile"))
                        inMovies[1] = MovieCreator.build(outputFile!!.absolutePath)
                        val videoTracks = LinkedList<Track>()
                        val audioTracks = LinkedList<Track>()
                        for (m in inMovies) {
                            for (t in m!!.tracks) {
                                if (t.handler == "soun") {
                                    audioTracks.add(t)
                                }
                                if (t.handler == "vide") {
                                    videoTracks.add(t)
                                }
                            }
                        }
                        val result = Movie()

                        if (videoTracks.size > 0)
                            result.addTrack(AppendTrack(*videoTracks.toTypedArray()))

//                        if (audioTracks.size > 0)
//                            result.addTrack(AppendTrack(*audioTracks.toTypedArray()))


                        val out = DefaultMp4Builder()
                                .build(result) as BasicContainer

                        val fc = RandomAccessFile(String.format(Environment.getExternalStorageDirectory().toString() + "/wishbyvideo.mp4"), "rw").channel
                        out.writeContainer(fc)
                        fc.close()

                        var mFileName = Environment.getExternalStorageDirectory()
                                .absolutePath
                        mFileName += "/wishbyvideo.mp4"
                        Log.d("whatisfileresult", mFileName)
                        Toast.makeText(this@MainActivity, "Encoding complete!", Toast.LENGTH_LONG).show()
                    }
                }
            })
            (1..100).forEach {
                bitmapToVideoEncoder.queueFrame(picBitmap)
            }
            bitmapToVideoEncoder.startEncoding(picBitmap.width, picBitmap.height, file)

        }

        btn_merge.setOnClickListener {
            Log.d("whatis", selectedVideoPath1)
            val inMovies = arrayOfNulls<Movie>(2)
            inMovies[0] = MovieCreator.build(getRealPathFromURI(videoUri1!!, "firstFile"))
            inMovies[1] = MovieCreator.build(getRealPathFromURI(videoUri2!!, "secondFile"))
            val videoTracks = LinkedList<Track>()
            val audioTracks = LinkedList<Track>()
            for (m in inMovies) {
                for (t in m!!.tracks) {
                    if (t.handler == "soun") {
                        audioTracks.add(t)
                    }
                    if (t.handler == "vide") {
                        videoTracks.add(t)
                    }
                }
            }
            val result = Movie()

            if (videoTracks.size > 0)
                result.addTrack(AppendTrack(*videoTracks.toTypedArray()))

            if (audioTracks.size > 0)
                result.addTrack(AppendTrack(*audioTracks.toTypedArray()))


            val out = DefaultMp4Builder()
                    .build(result) as BasicContainer

            val fc = RandomAccessFile(String.format(Environment.getExternalStorageDirectory().toString() + "/wishbyvideo.mp4"), "rw").channel
            out.writeContainer(fc)
            fc.close()

            var mFileName = Environment.getExternalStorageDirectory()
                    .absolutePath
            mFileName += "/wishbyvideo.mp4"
            Log.d("whatisfileresult", mFileName)
        }

    }

    fun getRealPathFromURI(contentUri: Uri, fileName: String): String {
        val file = File(cacheDir, fileName)
        val input = contentResolver.openInputStream(contentUri)
        val out = FileOutputStream(file)
        input.copyTo(
                out
        )
        input.close()
        out.flush()
        out.close()
        return file.absolutePath
    }


    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_TAKE_GALLERY_VIDEO_1) {
                val selectedImageUri = data!!.data
                // MEDIA GALLERY
                selectedVideoPath1 = selectedImageUri.path
                videoUri1 = selectedImageUri
                if (selectedVideoPath1 != null) {
                    video1.setVideoURI(selectedImageUri)
                    video1.start()
                }
            } else if (requestCode == REQUEST_TAKE_GALLERY_VIDEO_2) {
                val selectedImageUri = data!!.data
                // MEDIA GALLERY
                selectedVideoPath2 = selectedImageUri.path
                videoUri2 = selectedImageUri
                if (selectedVideoPath2 != null) {
                    video2.setVideoURI(selectedImageUri)
                    video2.start()
                }
            } else if (requestCode == REQUEST_TAKE_GALLERY_IMAGE) {
                selectedImagePath = data!!.data.path
                imageUri = data!!.data
            }
        }
    }

}
